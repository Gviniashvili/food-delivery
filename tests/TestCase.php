<?php

namespace Tests;

use App\Models\Restaurant;
use App\Services\OrderService;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Support\Facades\Artisan;

abstract class TestCase extends BaseTestCase {
  use CreatesApplication, DatabaseMigrations;

  public function setUp(): void {
    parent::setUp();
    Artisan::call('migrate:fresh --seed');
    Artisan::call('passport:install');
  }

  protected function createRandomOrder($user) {
    $restaurant = Restaurant::all()->first();
    $meals = $restaurant->meals()->inRandomOrder()->limit(3)->get();

    $orderItems = [];
    foreach($meals as $meal) {
      $orderItems[] = [ 'id' => $meal->id, 'quantity' => rand(1, 5) ];
    }

    $orderService = new OrderService();
    return $orderService->createOrder($orderItems, $user);
  }
}
