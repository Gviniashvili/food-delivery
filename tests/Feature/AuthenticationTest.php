<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
  /**
   * @test
   */
  public function it_should_authenticate_users()
  {
    $payload = ['email' => 'invalid@mail.com', 'password' => '123456'];

    $this->json('POST', 'api/login', $payload)
      ->assertStatus(200)
      ->assertJson([
        'error' => 'Invalid credentials',
      ]);

    $user = User::factory()->make(['password' => bcrypt('123456')]);
    $user->save();
    $payload = [
      'email' => $user->email,
      'password' => '123456'
    ];

    $this->json('POST', '/api/login', $payload)
      ->assertStatus(200)
      ->assertJsonStructure([
        'access_token',
        'user' => [
          'id',
          'name',
          'blocked',
          'is_admin',
        ],
      ]);
  }

  /**
   * @test
   */
  public function it_should_sign_out_users()
  {
    $user = User::factory()->make(['password' => bcrypt('123456')]);
    $user->save();
    $payload = ['email' => $user->email, 'password' => '123456'];

    $response = $this->json('POST', '/api/login', $payload)
      ->assertStatus(200)
      ->assertJsonStructure([
        'access_token',
        'user' => [
          'id',
          'name',
          'blocked',
          'is_admin',
        ],
      ])->getContent();

    $response = json_decode($response);

    $token = $response->access_token;
    $headers = ['Authorization' => "Bearer $token"];
    $this->json('get', '/api/restaurants', [], $headers)->assertStatus(200);
    $this->json('post', '/api/logout', [], $headers)->assertStatus(200);

    $token = $user->tokens->first();
    $this->assertEquals(true, $token->revoked);
  }

  /**
   * @test
   */
  public function it_should_register_users()
  {
    $payload = [
      'name' => 'John Smith',
      'email' => 'john@fooddelivery.ge',
      'password' => '123456',
      'password_confirmation' => '123456',
    ];

    $this->json('post', '/api/register', $payload)
      ->assertStatus(200)
      ->assertJsonStructure([
        'access_token',
        'user' => [
          'id',
          'name',
          'blocked',
          'is_admin',
        ],
      ]);
  }
}