<?php

namespace Tests\Feature;

use App\Models\Restaurant;
use App\Models\User;
use Tests\TestCase;

class AdminUserTest extends TestCase {
  /**
   * @test
   */
  public function it_should_list_restaurants_and_meals() {
    $headers = $this->getAdminUserHeaders();

    $this->json('get', '/api/admin/restaurants', [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        ['id', 'name', 'description'],
      ]);

    $restaurant = Restaurant::all()->first();
    $this->json('get', '/api/admin/meals?restaurant_id=' . $restaurant->id, [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'restaurant' => ['id', 'name', 'description'],
        'meals' => [
          ['id', 'name', 'description', 'price']
        ]
      ]);
  }

  /**
   * @test
   */
  public function it_should_crud_restaurants() {
    $headers = $this->getAdminUserHeaders();

    $response = $this->json('post', '/api/admin/restaurants', [
      'name' => 'Name',
      'description' => 'Description'
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
        'restaurant' => ['id', 'name', 'description'],
      ])
    ->getContent();

    $response = json_decode($response);
    $restaurant = $response->restaurant;

    $this->assertEquals('Name', $restaurant->name);
    $this->assertEquals('Description', $restaurant->description);

    $response = $this->json('put', '/api/admin/restaurants/'.$restaurant->id, [
      'name' => 'Name 2',
      'description' => 'Description 2'
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
        'restaurant' => ['id', 'name', 'description'],
      ])
      ->getContent();

    $response = json_decode($response);
    $restaurant = $response->restaurant;

    $this->assertEquals('Name 2', $restaurant->name);
    $this->assertEquals('Description 2', $restaurant->description);

    $this->json('delete', '/api/admin/restaurants/'.$restaurant->id, [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
      ]);

    $this->json('put', '/api/admin/restaurants/'.$restaurant->id, [
      'name' => 'Name 3',
      'description' => 'Description 3'
    ], $headers)
      ->assertStatus(404);
  }

  /**
   * @test
   */
  public function it_should_crud_meals() {
    $headers = $this->getAdminUserHeaders();
    $restaurant = Restaurant::all()->first();

    $response = $this->json('post', '/api/admin/meals', [
      'name' => 'Name',
      'description' => 'Description',
      'price' => 2200,
      'restaurant_id' => $restaurant->id
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
        'meal' => ['id', 'name', 'description', 'price'],
      ])
    ->getContent();

    $response = json_decode($response);
    $meal = $response->meal;

    $this->assertEquals('Name', $meal->name);
    $this->assertEquals('Description', $meal->description);
    $this->assertEquals(2200, $meal->price);

    $response = $this->json('put', '/api/admin/meals/'.$meal->id, [
      'name' => 'Name 2',
      'description' => 'Description 2',
      'price' => 5450
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
        'meal' => ['id', 'name', 'description', 'price'],
      ])
      ->getContent();

    $response = json_decode($response);
    $meal = $response->meal;

    $this->assertEquals('Name 2', $meal->name);
    $this->assertEquals('Description 2', $meal->description);
    $this->assertEquals(5450, $meal->price);

    $this->json('delete', '/api/admin/meals/'.$meal->id, [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'success',
      ]);

    $this->json('put', '/api/admin/meals/'.$meal->id, [
      'name' => 'Name 3',
      'description' => 'Description 3',
      'price' => 5450
    ], $headers)
      ->assertStatus(404);
  }

  private function getAdminUserHeaders(): array {
    $user = User::whereEmail('admin@fooddelivery.ge')->first();
    $token = $user->createToken('webApp', [ 'manage-restaurants' ])->accessToken;
    return ['Authorization' => "Bearer $token"];
  }
}
