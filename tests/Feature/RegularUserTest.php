<?php

namespace Tests\Feature;

use App\Models\Order;
use App\Models\Restaurant;
use App\Models\User;
use App\Services\OrderService;
use Tests\TestCase;

class RegularUserTest extends TestCase {
  /**
   * @test
   */
  public function it_should_list_restaurants_and_meals() {
    $user = User::whereEmail('user@fooddelivery.ge')->first();
    $token = $user->createToken('webApp')->accessToken;
    $headers = ['Authorization' => "Bearer $token"];

    $this->json('get', '/api/restaurants', [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        ['id', 'name', 'description'],
      ]);

    $restaurant = Restaurant::all()->first();
    $this->json('get', '/api/meals?restaurant_id=' . $restaurant->id, [], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'restaurant' => ['id', 'name', 'description'],
        'meals' => [
          ['id', 'name', 'description', 'price']
        ]
      ]);
  }

  /**
   * @test
   */
  public function it_should_create_an_order() {
    $user = User::whereEmail('user@fooddelivery.ge')->first();
    $token = $user->createToken('webApp')->accessToken;
    $headers = ['Authorization' => "Bearer $token"];

    $restaurant = Restaurant::all()->first();
    $mealIds = $restaurant->meals()->inRandomOrder()->limit(3)->get()->pluck('id')->toArray();
    $orderData = [
      'meals' => []
    ];
    foreach ($mealIds as $mealId) {
      $orderData['meals'][] = ['id' => $mealId, 'quantity' => rand(1, 5)];
    }

    $this->json('post', '/api/orders', $orderData, $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'order' => [
          'id', 'date', 'data', 'total_amount', 'status', 'actions', 'statusUpdates', 'user',
        ],
      ])
    ->getContent();
  }

  /**
   * @test
   */
  public function it_should_update_order_status() {
    $user = User::whereEmail('user@fooddelivery.ge')->first();
    $token = $user->createToken('webApp')->accessToken;
    $headers = ['Authorization' => "Bearer $token"];
    $order = $this->createRandomOrder($user);

    $response = $this->json('put', '/api/orders/'.$order->id, [
      'status' => 'cancelled'
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'order' => [
          'id', 'date', 'data', 'total_amount', 'status', 'actions', 'statusUpdates', 'user',
        ],
      ])
      ->getContent();

    $response = json_decode($response);
    $this->assertEquals('Cancelled', $response->order->status);

    $order->fill([ 'status' => Order::DELIVERED ])->save();

    $response = $this->json('put', '/api/orders/'.$order->id, [
      'status' => 'received'
    ], $headers)
      ->assertStatus(200)
      ->assertJsonStructure([
        'order' => [
          'id', 'date', 'data', 'total_amount', 'status', 'actions', 'statusUpdates', 'user',
        ],
      ])
      ->getContent();

    $response = json_decode($response);
    $this->assertEquals('Received', $response->order->status);
  }
}
