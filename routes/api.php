<?php

use App\Http\Controllers\Api\Admin\UserController;
use App\Http\Controllers\Api\HomeController;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\Api\OrderController;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([], function (Router $router) {
  $router->post('/login', [AuthController::class, 'login'])->name('api.login');
  $router->post('/register', [AuthController::class, 'register'])->name('api.register');
});

Route::group(['middleware' => ['auth:api']], function (Router $router) {
  $router->post('/logout', [AuthController::class, 'logOut'])->name('api.logout');

  $router->get('/restaurants', [HomeController::class, 'restaurants'])->name('api.restaurants');
  $router->get('/meals', [HomeController::class, 'meals'])->name('api.meals');

  $router->get('/orders', [OrderController::class, 'index'])->name('api.orders.index');
  $router->post('/orders', [OrderController::class, 'store'])->name('api.orders.store');
  $router->put('/orders/{order}', [OrderController::class, 'updateStatus'])->name('api.orders.update-status');

  $router->resource('/admin/restaurants', 'App\Http\Controllers\Api\Admin\RestaurantController', [
    'names' => [
      'index' => 'api.admin.restaurants.index',
      'store' => 'api.admin.restaurants.store',
      'update' => 'api.admin.restaurants.update',
      'destroy' => 'api.admin.restaurants.delete'
    ]
  ])->middleware('scope:manage-restaurants');

  $router->resource('/admin/meals', 'App\Http\Controllers\Api\Admin\MealController', [
    'names' => [
      'index' => 'api.admin.meals.index',
      'store' => 'api.admin.meals.store',
      'update' => 'api.admin.meals.update',
      'destroy' => 'api.admin.meals.delete'
    ]
  ])->middleware('scope:manage-restaurants');

  $router->get('/admin/orders', [\App\Http\Controllers\Api\Admin\OrderController::class, 'index'])
    ->name('api.admin.orders.index')->middleware('scope:manage-restaurants');

  $router->put(
    '/admin/orders/{order}',
    [\App\Http\Controllers\Api\Admin\OrderController::class, 'updateStatus']
  )->name('api.admin.orders.update-status')->middleware('scope:manage-restaurants');

  $router->put(
    '/admin/users/{user}/block',
    [UserController::class, 'block']
  )->name('api.admin.users.block')->middleware('scope:manage-restaurants');

  $router->put(
    '/admin/users/{user}/unblock',
    [UserController::class, 'unblock']
  )->name('api.admin.users.unblock')->middleware('scope:manage-restaurants');
});
