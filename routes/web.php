<?php

use Illuminate\Routing\Router;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group([], function (Router $router) {
  $webUrls = [ '/', '/restaurant/{id}', '/cart', '/orders' ];

  foreach($webUrls as $url) {
    $router->get($url, function () {
      return view('welcome');
    });
  }
});
