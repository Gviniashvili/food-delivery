import React, { useContext, useEffect, useState } from 'react';
import { SettingsStore } from '../../stores/Settings';
import { OrdersApi } from '../../api/Admin/Orders';
import { UsersApi } from '../../api/Admin/Users';
import ConfirmDialog from '../ConfirmDialog';
import { Order } from '../Orders/Order';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

export const AdminOrders = () => {
  const settingsStore = useContext(SettingsStore);
  const [orders, setOrders] = useState([]);
  const [statusUpdateConfirmOpen, setStatusUpdateConfirmOpen] = useState(false);
  const [newStatus, setNewStatus] = useState(null);
  const [selectedOrderId, setSelectedOrderId] = useState(null);

  const [orderHistoryOpen, setOrderHistoryOpen] = useState(false);
  const [orderStatusHistory, setOrderStatusHistory] = useState([]);

  const [selectedUser, setSelectedUser] = useState(null);
  const [blockUserConfirmOpen, setBlockUserConfirmOpen] = useState(false);
  const [unblockUserConfirmOpen, setUnblockUserConfirmOpen] = useState(false);

  useEffect(function () {
    OrdersApi.index(settingsStore.state.user.access_token)
      .then((list) => {
        setOrders(list);
      })
      .catch(() => console.error('Unable to load restaurants'));
  }, []);

  const onConfirmStatusUpdate = () => {
    OrdersApi.updateStatus(selectedOrderId, newStatus, settingsStore.state.user.access_token)
      .then((r) => {
        const newList = orders.map((o) => (o.id === r.order.id ? r.order : o));
        setOrders([...newList]);
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  const onConfirmBlockUser = () => {
    UsersApi.block(selectedUser.id, settingsStore.state.user.access_token)
      .then((r) => {
        const newList = orders.map((o) => {
          if(o.user.id === selectedUser.id) o.user = r.user;
          return o;
        });
        setOrders([...newList]);
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  const onConfirmUnblockUser = () => {
    UsersApi.unblock(selectedUser.id, settingsStore.state.user.access_token)
      .then((r) => {
        const newList = orders.map((o) => {
          if(o.user.id === selectedUser.id) o.user = r.user;
          return o;
        });
        setOrders([...newList]);
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  return (
    <>
      <section className="standard mt-4 mb-4">
        <div className="section-heading">
          <h2>Order history</h2>
        </div>

        <div className="section-body">
          {orders.length ? (
            <table className="table table-striped orders">
              <thead>
              <tr>
                <th>User</th>
                <th>Date</th>
                <th>Items</th>
                <th>Total Price</th>
                <th>Status</th>
                <th>History</th>
                <th>Action</th>
              </tr>
              </thead>

              <tbody>
              {orders.map((order) => (
                <Order
                  key={order.id}
                  showUser={true}
                  order={order}
                  onProcessing={() => {
                    setStatusUpdateConfirmOpen(true);
                    setNewStatus('processing');
                    setSelectedOrderId(order.id);
                  }}
                  onInRoute={() => {
                    setStatusUpdateConfirmOpen(true);
                    setNewStatus('in_route');
                    setSelectedOrderId(order.id);
                  }}
                  onDelivered={() => {
                    setStatusUpdateConfirmOpen(true);
                    setNewStatus('delivered');
                    setSelectedOrderId(order.id);
                  }}
                  onViewHistory={() => {
                    setOrderHistoryOpen(true);
                    setOrderStatusHistory(order.statusUpdates);
                  }}
                  onBlockUser={() => {
                    setSelectedUser(order.user);
                    setBlockUserConfirmOpen(true);
                  }}
                  onUnblockUser={() => {
                    setSelectedUser(order.user);
                    setUnblockUserConfirmOpen(true);
                  }}
                />
              ))}
              </tbody>
            </table>
          ) : (
            <div className="text-center mb-4">
              <h3 className="mt-2">Orders not found</h3>
            </div>
          )}
        </div>
      </section>

      <ConfirmDialog
        title={'Update order status'}
        open={statusUpdateConfirmOpen}
        setOpen={setStatusUpdateConfirmOpen}
        onConfirm={onConfirmStatusUpdate}
        yesBtnText={'Confirm'}
      >
        Do you want to mark this order as <strong>{newStatus ? newStatus.replace('_', ' ') : ''}</strong>?
      </ConfirmDialog>

      <ConfirmDialog
        title={'Order status updates'}
        open={orderHistoryOpen}
        setOpen={setOrderHistoryOpen}
        cancelBtnText={'Close'}
      >
        <dl className="order-status-history">
          {orderStatusHistory.map((item, i) => (
            <div className="d-flex justify-content-between" key={i}>
              <dt>{item.status}</dt>
              <dd>{item.date}</dd>
            </div>
          ))}
        </dl>
      </ConfirmDialog>

      <ConfirmDialog
        title={'Block user'}
        open={blockUserConfirmOpen}
        setOpen={setBlockUserConfirmOpen}
        onConfirm={onConfirmBlockUser}
        yesBtnText={'Confirm'}
      >
        Do you want to block <strong>{selectedUser?.name || ''}</strong>?
      </ConfirmDialog>

      <ConfirmDialog
        title={'Unblock user'}
        open={unblockUserConfirmOpen}
        setOpen={setUnblockUserConfirmOpen}
        onConfirm={onConfirmUnblockUser}
        yesBtnText={'Confirm'}
      >
        Do you want to unblock <strong>{selectedUser?.name || ''}</strong>?
      </ConfirmDialog>
    </>
  );
};
