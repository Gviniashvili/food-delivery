import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt, faCheck, faPlus } from '@fortawesome/free-solid-svg-icons';

export const View = ({ editMode, name, description, price, onEdit, onDelete, addToCart, onAddToCart, isInCart }) => {
  const [showOptions, setShowOptions] = useState(false);
  const [quantity, setQuantity] = useState(1);

  return (
    <tr className="meal">
      <td>
        <div className="d-flex align-items-center">
          <img src="img/placeholder-image.jpg" alt="thumb" className="d-none d-sm-block" />
          <div className="info">
            <h5>{name}</h5>
            <p>{description}</p>
          </div>
        </div>
      </td>
      <td>
        <strong>${price}</strong>
      </td>
      <td>
        {addToCart && (
          <div className="add-to-cart">
            {isInCart ? (
              <button className="success">
                <FontAwesomeIcon icon={faCheck} />
              </button>
            ) : (
              <button onClick={() => setShowOptions(!showOptions)}>
                <FontAwesomeIcon icon={faPlus} />
              </button>
            )}

            {showOptions && (
              <div className="options">
                <h6>Select quantity</h6>
                <div className="quantity-selector">
                  <button disabled={quantity === 1} className="btn btn-secondary" onClick={() => setQuantity(quantity - 1)}>
                    -
                  </button>
                  <span>{quantity}</span>
                  <button className="btn btn-secondary" onClick={() => setQuantity(quantity + 1)}>
                    +
                  </button>
                </div>
                <button
                  className="btn btn-secondary"
                  onClick={() => {
                    setShowOptions(false);
                    onAddToCart(quantity);
                  }}
                >
                  Add to cart
                </button>
              </div>
            )}
          </div>
        )}

        {editMode && (
          <div className="d-flex actions">
            <button onClick={onEdit}>
              <FontAwesomeIcon icon={faPencilAlt} />
            </button>
            <button onClick={onDelete}>
              <FontAwesomeIcon icon={faTrashAlt} />
            </button>
          </div>
        )}
      </td>
    </tr>
  );
};
