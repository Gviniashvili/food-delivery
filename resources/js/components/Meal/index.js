import React, { useContext, useEffect, useState } from 'react';
import { SettingsStore } from '../../stores/Settings';
import { MealsApi as AdminMealsApi } from '../../api/Admin/Meals';
import { Meals as MealsApi } from '../../api/Meals';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { MealContainer } from './Controller';
import { useHistory, useParams } from 'react-router-dom';
import { Loading } from '../Loading';
import { Summary } from '../Cart/Summary';
import ConfirmDialog from '../ConfirmDialog';
import { OrdersApi } from '../../api/Orders';
import { MealEditModal } from './EditModal';

export const Meals = ({ editMode }) => {
  const { id: restaurantId } = useParams();

  const settingsStore = useContext(SettingsStore);

  const [isLoading, setIsLoading] = useState(true);
  const [restaurant, setRestaurant] = useState(null);
  const [meals, setMeals] = useState([]);
  const [confirmOrderOpen, setConfirmOrderOpen] = useState(false);
  const [editModalOpen, setEditModalOpen] = useState(false);
  const [editingMeal, setEditingMeal] = useState(null);

  const cart = (settingsStore.state.cart && settingsStore.state.cart[restaurantId]) || [];

  const API = editMode ? AdminMealsApi : MealsApi;

  const routeHistory = useHistory();

  useEffect(function () {
    API.index(restaurantId, settingsStore.state.user.access_token)
      .then((r) => {
        if (r && r.restaurant) setRestaurant(r.restaurant);
        if (r && r.meals) setMeals(r.meals);
        setIsLoading(false);
      })
      .catch(() => console.error('Unable to load meals'));
  }, []);

  const addNewMeal = () => {
    setEditingMeal({ id: null, name: '', description: '', price: '' });
    setEditModalOpen(true);
  };

  const handleMealDelete = async (id) => {
    await AdminMealsApi.remove(id, settingsStore.state.user.access_token);
    const newList = meals.filter((r) => r.id !== id);
    setMeals([...newList]);
  };

  const getMealById = (id) => {
    const filtered = meals.filter((r) => r.id === id);
    return filtered[0] || null;
  };

  const addToCart = (mealId, quantity) => {
    const meal = getMealById(mealId);
    meal.restaurant = restaurant;

    const filtered = cart.filter((cartItem) => cartItem.id === mealId);
    if (filtered.length) throw 'This item is already added to cart';

    meal.quantity = quantity;
    cart.push(meal);
    const wholeCart = settingsStore.state.cart || {};
    wholeCart[restaurantId] = cart;
    settingsStore.dispatch({ type: 'UPDATE', cart: { ...wholeCart } });
  };

  const isInCart = (id) => {
    const filtered = cart.filter((cartItem) => cartItem.id === id);
    return filtered.length > 0;
  };

  const onRemoveItem = (id) => {
    const newCart = cart.filter((meal) => meal.id !== id);
    const wholeCart = settingsStore.state.cart || {};
    wholeCart[restaurantId] = newCart;
    settingsStore.dispatch({ type: 'UPDATE', cart: { ...wholeCart } });
  };

  const onConfirmOrder = () => {
    const items = cart.map((item) => ({
      id: item.id,
      quantity: item.quantity,
    }));
    OrdersApi.create({ meals: items }, settingsStore.state.user.access_token)
      .then((r) => {
        const orderedItemIds = r.order.data.meals.map((item) => item.id);
        const newList = cart.filter((item) => orderedItemIds.indexOf(item.id) === -1);
        settingsStore.dispatch({ type: 'UPDATE', cart: newList });
        routeHistory.push('/orders');
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  const startEditing = (id) => {
    setEditingMeal(getMealById(id));
    setEditModalOpen(true);
  };

  const onUpdate = (newMeal) => {
    const exists = getMealById(newMeal.id);
    if (exists) {
      const newList = meals.map((m) => (m.id === newMeal.id ? newMeal : m));
      setMeals([...newList]);
    } else {
      setMeals([newMeal, ...meals]);
    }
    setEditingMeal(null);
  };

  return isLoading ? (
    <Loading />
  ) : (
    <>
      <div className="row">
        <div className={editMode ? 'col-12' : 'col-md-8 order-1 order-md-0'}>
          <section className="standard mt-4 mb-4">
            <div className="section-heading">
              <h2>Menu</h2>
            </div>

            <div className="section-body">
              <div className="row justify-content-between">
                <div className="col-md-6">
                  <h3>{restaurant.name}</h3>
                  <p>{restaurant.description}</p>
                </div>

                {editMode && (
                  <div className="col-md-6 text-md-right">
                    <button className="btn btn-primary mb-3" onClick={addNewMeal}>
                      <FontAwesomeIcon icon={faPlus} /> Add a Meal
                    </button>
                  </div>
                )}
              </div>

              <table className="table table-striped meals">
                <thead>
                  <tr>
                    <th>Item</th>
                    <th>Price</th>
                    <th>Order</th>
                  </tr>
                </thead>

                <tbody>
                  {meals.map((item) => (
                    <MealContainer
                      key={item.id}
                      editMode={editMode}
                      isNew={!item.id}
                      name={item.name}
                      description={item.description}
                      price={item.price}
                      onEdit={() => startEditing(item.id)}
                      onDelete={() => handleMealDelete(item.id)}
                      onAddToCart={(quantity) => addToCart(item.id, quantity)}
                      isInCart={isInCart(item.id)}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </section>
        </div>

        {!editMode && (
          <div className="col-md-4 order-0 order-md-1 mt-4 mt-md-0">
            <Summary items={cart} onRemoveItem={onRemoveItem} onCreateOrder={() => setConfirmOrderOpen(true)} />
          </div>
        )}
      </div>

      <ConfirmDialog
        open={confirmOrderOpen}
        setOpen={setConfirmOrderOpen}
        onConfirm={onConfirmOrder}
        yesBtnText={'Confirm'}
        extraClass={'naked'}
      >
        <div className="order-confirm-summary">
          <Summary title={'Confirm your order'} items={cart} withAction={false} />
        </div>
      </ConfirmDialog>

      {editingMeal && (
        <MealEditModal
          restaurantId={restaurantId}
          meal={editingMeal}
          open={editModalOpen}
          setOpen={setEditModalOpen}
          onUpdate={onUpdate}
          onCancel={() => setEditingMeal(null)}
        />
      )}
    </>
  );
};
