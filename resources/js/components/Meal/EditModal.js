import React, { useRef, useState, useEffect, useContext } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/free-solid-svg-icons';
import ConfirmDialog from '../ConfirmDialog';
import { MealsApi as AdminMealsApi } from '../../api/Admin/Meals';
import { SettingsStore } from '../../stores/Settings';

export const MealEditModal = ({ meal, restaurantId, open, setOpen, onUpdate, onCancel }) => {
  const settingsStore = useContext(SettingsStore);

  const [error, setError] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [name, setName] = useState(meal.name);
  const [description, setDescription] = useState(meal.description);
  const [price, setPrice] = useState(meal.price ? (meal.price / 100).toFixed(2) : 0);

  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();

    const newValues = {
      id: meal.id,
      name: name,
      price: price * 100,
      description: description,
    };

    if (!meal.id) {
      setIsLoading(true);
      AdminMealsApi.create(
        {
          ...newValues,
          restaurant_id: restaurantId,
        },
        settingsStore.state.user.access_token
      )
        .then((result) => {
          setOpen(false);
          onUpdate(result.meal);
          setIsLoading(false);
        })
        .catch((err) => {
          setError(err.error || 'An error occurred, please try again later');
          setIsLoading(false);
        });
    } else {
      setIsLoading(true);
      AdminMealsApi.update({ ...newValues }, settingsStore.state.user.access_token)
        .then((result) => {
          setOpen(false);
          onUpdate(result.meal);
          setIsLoading(false);
        })
        .catch((err) => {
          setError(err.error || 'An error occurred, please try again later');
          setIsLoading(false);
        });
    }
  };

  return (
    <ConfirmDialog
      open={open}
      setOpen={setOpen}
      yesBtnText={'Confirm'}
      extraClass={'edit-meal-form'}
      withoutActions={true}
      onCancel={onCancel}
    >
      <form onSubmit={onSubmit}>
        {isLoading && <div className="loading-mask" />}

        <div className="form-group">
          <label htmlFor="meal-name" className="control-label">
            Name
          </label>
          <input
            id="meal-name"
            ref={inputRef}
            type="text"
            className="form-control"
            defaultValue={name}
            disabled={isLoading}
            required
            onChange={(e) => setName(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="meal-price" className="control-label">
            Price
          </label>
          <input
            type="number"
            id="meal-price"
            min="0"
            step="0.01"
            className="form-control"
            defaultValue={price}
            disabled={isLoading}
            required
            onChange={(e) => setPrice(e.target.value)}
          />
        </div>

        <div className="form-group">
          <label htmlFor="meal-description" className="control-label">
            Description
          </label>
          <textarea
            rows="4"
            id="meal-description"
            className="form-control"
            defaultValue={description}
            disabled={isLoading}
            required
            onChange={(e) => setDescription(e.target.value)}
          />
        </div>

        {error && <div className="text-danger">{error}</div>}

        <div className="form-group text-center">
          <button
            type="button"
            className="btn btn-default"
            onClick={() => {
              setOpen(false);
              onCancel && onCancel();
            }}
            disabled={isLoading}
          >
            Cancel
          </button>

          <button type="submit" className="btn btn-primary" disabled={isLoading}>
            <FontAwesomeIcon icon={faSave} /> Save
          </button>
        </div>
      </form>
    </ConfirmDialog>
  );
};
