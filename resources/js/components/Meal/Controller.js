import React, { useContext, useState } from 'react';
import { View as MealView } from './View';
import ConfirmDialog from '../ConfirmDialog';
import { Edit as MealEdit } from './Edit';
import { SettingsStore } from '../../stores/Settings';

export const MealContainer = ({
  editMode,
  name,
  description,
  price,
  onEdit,
  onDelete,
  onAddToCart,
  isInCart,
}) => {
  const [confirmOpen, setConfirmOpen] = useState(false);

  return (
    <>
      <MealView
        editMode={editMode}
        name={name}
        description={description}
        price={price ? (price / 100).toFixed(2) : 0}
        onEdit={onEdit}
        onDelete={() => setConfirmOpen(true)}
        addToCart={!editMode}
        onAddToCart={onAddToCart}
        isInCart={isInCart}
      />

      <ConfirmDialog
        title={'Deleting ' + name}
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={onDelete}
        yesBtnText={'Delete'}
      >
        Are you sure you want to delete meal {name}?
      </ConfirmDialog>
    </>
  );
};
