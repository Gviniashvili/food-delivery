import React, { useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/free-solid-svg-icons';

export const Edit = ({
  name,
  description,
  price,
  error,
  isLoading,
  onCancelEdit,
  onConfirmEdit,
  onNameChange,
  onDescriptionChange,
  onPriceChange,
}) => {
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    onConfirmEdit();
  };

  return (
    <tr className="meal">
      <td>
        <form className="meal" onSubmit={onSubmit}>
          <div className="d-flex align-items-center">
            <img src="img/placeholder-image.jpg" alt="thumb" />
            <div className="info flex-grow-1">
              <div className="form-group">
                <input
                  ref={inputRef}
                  type="text"
                  className="form-control"
                  defaultValue={name}
                  disabled={isLoading}
                  required
                  onChange={(e) => onNameChange(e.target.value)}
                />
              </div>

              <div className="form-group">
                <textarea
                  rows="2"
                  className="form-control"
                  defaultValue={description}
                  disabled={isLoading}
                  required
                  onChange={(e) => onDescriptionChange(e.target.value)}
                />
              </div>
            </div>
          </div>
        </form>
      </td>

      <td>
        <input
          type="text"
          className="form-control"
          defaultValue={price}
          disabled={isLoading}
          required
          onChange={(e) => onPriceChange(e.target.value)}
        />
      </td>

      <td>
        <button type="submit" className="btn btn-primary" disabled={isLoading}>
          <FontAwesomeIcon icon={faSave} /> Save
        </button>
        <button className="btn btn-default" onClick={onCancelEdit} disabled={isLoading}>
          Cancel
        </button>

        {error && <div className="text-danger">{error}</div>}
      </td>
    </tr>
  );
};
