import React, {useContext, useState} from 'react';
import {SettingsStore} from "../stores/Settings";
import UserDropdown from "./UserDropdown";
import { Link } from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faBars} from "@fortawesome/free-solid-svg-icons";
import AuthApi from "../api/Auth";

function Header() {
  const settingsStore = useContext(SettingsStore);

  const [ mobileMenuOpen, setMobileMenuOpen ] = useState(false);

  const isRegularUser = settingsStore.state.user && !settingsStore.state.user.is_admin;
  const isAdmin = settingsStore.state.user && settingsStore.state.user.is_admin;

  const onLogOut = () => {
    AuthApi.logout(settingsStore.state.user.access_token).then(function() {
      settingsStore.dispatch({ type: 'UPDATE', user: null, cart: null });
    });
  }

  return (
    <>
      <header>
        <div className="container">
          <div className="row">
            <div className="col-6 d-flex">
              <Link to="/" className="logo">FoodDelivery</Link>
            </div>
            <div className="col-6">
              <nav className="d-none d-md-flex">
                <ul>
                  {isAdmin && <li><Link to="/orders">Order History</Link></li>}
                  {isRegularUser && <li><Link to="/orders">Your Orders</Link></li>}
                  {settingsStore.state.user ? <UserDropdown onLogOut={onLogOut} /> : null}
                </ul>
              </nav>

              {settingsStore.state.user && <div className="mobile-menu-toggle">
                <button className="d-block d-md-none" onClick={() => setMobileMenuOpen(!mobileMenuOpen)}>
                  <FontAwesomeIcon icon={faBars} />
                </button>

                {mobileMenuOpen && <div className="mobile-menu">
                  <nav>
                    <ul>
                      {isAdmin && <li><Link onClick={() => setMobileMenuOpen(false)} to="/orders">Order History</Link></li>}
                      {isRegularUser && <li><Link onClick={() => setMobileMenuOpen(false)} to="/orders">Your Orders</Link></li>}
                      {isRegularUser && <li onClick={onLogOut}>Logout</li>}
                    </ul>
                  </nav>
                </div>}
              </div>}
            </div>
          </div>
        </div>
      </header>
    </>
  );
}

export default Header;
