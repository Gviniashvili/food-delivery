import React from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

const ConfirmDialog = (props) => {
  const {
    title,
    children,
    open,
    yesBtnText,
    cancelBtnText = 'Cancel',
    setOpen,
    onCancel,
    extraClass,
    withoutActions,
    onConfirm,
  } = props;
  return (
    <Dialog
      open={open}
      onClose={() => {
        setOpen(false);
        onCancel && onCancel();
      }}
      aria-labelledby="confirm-dialog"
      className={extraClass}
    >
      {title && <DialogTitle id="confirm-dialog">{title}</DialogTitle>}
      <DialogContent>{children}</DialogContent>
      {!withoutActions && (
        <DialogActions>
          <Button
            variant="contained"
            onClick={() => {
              setOpen(false);
              onCancel && onCancel();
            }}
            color="default"
          >
            {cancelBtnText}
          </Button>
          {yesBtnText && (
            <Button
              variant="contained"
              onClick={() => {
                setOpen(false);
                onConfirm();
              }}
              color="secondary"
            >
              {yesBtnText}
            </Button>
          )}
        </DialogActions>
      )}
    </Dialog>
  );
};
export default ConfirmDialog;
