import React, { useContext } from 'react';
import { Pricing } from '../Cart/Pricing';
import { getTotalPrice } from '../../helpers/getTotalPrice';
import orderStyles from '../../../css/modules/Order.module.css';
import { SettingsStore } from '../../stores/Settings';

export const Order = ({
  order,
  showUser,
  onCancel,
  onProcessing,
  onInRoute,
  onDelivered,
  onReceived,
  onViewHistory,
  onBlockUser,
  onUnblockUser,
}) => {
  const settingsStore = useContext(SettingsStore);
  const formattedTotalPrice = getTotalPrice(order.data.meals, true);

  const availableActions = order.actions || [];

  let statusClass = 'badge badge-secondary';
  if (order.status === 'Cancelled') statusClass = 'badge badge-danger';
  if (order.status === 'Processing') statusClass = 'badge badge-primary';
  if (order.status === 'In_route') statusClass = 'badge badge-warning';
  if (order.status === 'Delivered') statusClass = 'badge badge-success';
  if (order.status === 'Received') statusClass = 'badge badge-info';

  return (
    <tr>
      {showUser && (
        <td>
          <strong>{order.user.name}</strong>
          <div>
            {order.user.blocked ? (
              <a className={'text-danger ' + orderStyles.block} onClick={onUnblockUser}>
                (Unblock user)
              </a>
            ) : (
              <a className={'text-danger ' + orderStyles.block} onClick={onBlockUser}>
                (Block user)
              </a>
            )}
          </div>
        </td>
      )}
      <td>
        <strong>{order.date}</strong>
      </td>
      <td>
        <Pricing items={order.data.meals} withoutTotalPrice={true} />
      </td>
      <td><strong>{formattedTotalPrice}</strong></td>
      <td>
        <div className={statusClass + ' ' + orderStyles.status}>{order.status.replace('_', ' ')}</div>
      </td>
      <td>
        <a className="text-primary" onClick={onViewHistory}>
          View history
        </a>
      </td>
      <td>
        {availableActions.indexOf('cancel') > -1 && (
          <a className={'text-danger ' + orderStyles.actionBtn} onClick={onCancel}>
            Cancel Order
          </a>
        )}

        {availableActions.indexOf('processing') > -1 && (
          <button className={'btn btn-primary ' + orderStyles.actionBtn} onClick={onProcessing}>
            Start processing
          </button>
        )}

        {availableActions.indexOf('in_route') > -1 && (
          <button className={'btn btn-warning ' + orderStyles.actionBtn} onClick={onInRoute}>
            Mark as In Route
          </button>
        )}

        {availableActions.indexOf('delivered') > -1 && (
          <button className={'btn btn-success ' + orderStyles.actionBtn} onClick={onDelivered}>
            Mark as Delivered
          </button>
        )}

        {availableActions.indexOf('received') > -1 && (
          <button className={'btn btn-primary ' + orderStyles.actionBtn} onClick={onReceived}>
            Order received
          </button>
        )}
      </td>
    </tr>
  );
};
