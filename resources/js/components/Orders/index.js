import React, { useContext, useEffect, useState } from 'react';
import { SettingsStore } from '../../stores/Settings';
import { OrdersApi } from '../../api/Orders';
import ConfirmDialog from '../ConfirmDialog';
import { Order } from './Order';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';
import { MealContainer } from '../Meal/Controller';

export const Orders = () => {
  const settingsStore = useContext(SettingsStore);
  const [cancelConfirmOpen, setCancelConfirmOpen] = useState(false);
  const [receivedConfirmOpen, setReceivedConfirmOpen] = useState(false);
  const [orders, setOrders] = useState([]);
  const [selectedOrderId, setSelectedOrderId] = useState(null);

  const [orderHistoryOpen, setOrderHistoryOpen] = useState(false);
  const [orderStatusHistory, setOrderStatusHistory] = useState([]);

  useEffect(function () {
    OrdersApi.index(settingsStore.state.user.access_token)
      .then((list) => {
        setOrders(list);
      })
      .catch(() => console.error('Unable to load restaurants'));
  }, []);

  const onConfirmCancel = () => {
    OrdersApi.updateStatus(selectedOrderId, 'cancelled', settingsStore.state.user.access_token)
      .then((r) => {
        const newList = orders.map((o) => (o.id === r.order.id ? r.order : o));
        setOrders([...newList]);
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  const onConfirmReceived = () => {
    OrdersApi.updateStatus(selectedOrderId, 'received', settingsStore.state.user.access_token)
      .then((r) => {
        const newList = orders.map((o) => (o.id === r.order.id ? r.order : o));
        setOrders([...newList]);
      })
      .catch((err) => alert(err.error || 'An error occurred, please try again later'));
  };

  return (
    <>
      <section className="standard mt-4 mb-4">
        <div className="section-heading">
          <h2>Order history</h2>
        </div>

        <div className="section-body">
          {orders.length ? (
            <table className="table table-striped orders">
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Items</th>
                  <th>Total Price</th>
                  <th>Status</th>
                  <th>History</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                {orders.map((order) => (
                  <Order
                    key={order.id}
                    order={order}
                    onCancel={() => {
                      setCancelConfirmOpen(true);
                      setSelectedOrderId(order.id);
                    }}
                    onReceived={() => {
                      setReceivedConfirmOpen(true);
                      setSelectedOrderId(order.id);
                    }}
                    onViewHistory={() => {
                      setOrderHistoryOpen(true);
                      setOrderStatusHistory(order.statusUpdates);
                    }}
                  />
                ))}
              </tbody>
            </table>
          ) : (
            <div className="text-center mb-4">
              <h3 className="mt-2">You don't have any orders yet</h3>
              <Link to={'/'}>
                <button className="btn btn-primary mt-4">Order food from your favorite Restaurants</button>
              </Link>
            </div>
          )}
        </div>
      </section>

      <ConfirmDialog
        title={'Cancel order'}
        open={cancelConfirmOpen}
        setOpen={setCancelConfirmOpen}
        onConfirm={onConfirmCancel}
        yesBtnText={'Confirm'}
      >
        You are going to cancel this order. Do you want to continue?
      </ConfirmDialog>

      <ConfirmDialog
        title={'Order received?'}
        open={receivedConfirmOpen}
        setOpen={setReceivedConfirmOpen}
        onConfirm={onConfirmReceived}
        yesBtnText={'Confirm'}
      >
        You are going to mark this order as received. Do you want to continue?
      </ConfirmDialog>

      <ConfirmDialog
        title={'Order status updates'}
        open={orderHistoryOpen}
        setOpen={setOrderHistoryOpen}
        cancelBtnText={'Close'}
      >
        <dl className="order-status-history">
          {orderStatusHistory.map((item, i) => (
            <div className="d-flex justify-content-between" key={i}>
              <dt>{item.status}</dt>
              <dd>{item.date}</dd>
            </div>
          ))}
        </dl>
      </ConfirmDialog>
    </>
  );
};
