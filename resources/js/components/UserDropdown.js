import React, {useContext, useEffect, useState} from "react";
import {SettingsStore} from "../stores/Settings";

function UserDropdown({ onLogOut }) {
  const settingsStore = useContext(SettingsStore);

  const [ expanded, setExpanded ] = useState(false);

  useEffect(function() {
    document.addEventListener('click', onDocumentClick);

    return () => {
      document.removeEventListener('click', onDocumentClick);
    }
  }, []);

  function onDocumentClick() {
    setExpanded(false);
  }

  function onMenuClick(e) {
    e.stopPropagation();
    e.nativeEvent.stopImmediatePropagation();
    setExpanded(!expanded);
  }

  return (
    <li onClick={onMenuClick}>
      <img src="img/default-avatar.png" alt="{settingsStore.state.user.name}" className="avatar"/>
      {settingsStore.state.user.name}
      {expanded ? <div className="dropdown">
        <a onClick={onLogOut}>Log out</a>
      </div> : null}
    </li>
  );
}

export default UserDropdown;