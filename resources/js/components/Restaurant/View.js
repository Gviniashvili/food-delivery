import React, { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrashAlt } from '@fortawesome/free-solid-svg-icons';
import { Link } from 'react-router-dom';

export const View = ({ editMode, id, name, description, onEdit, onDelete }) => {
  return (
    <Link className="restaurant" to={'/restaurant/' + id}>
      <img src={'img/placeholder-image.jpg'} alt={name} />
      <h2>{name}</h2>
      <p>{description}</p>

      {editMode && (
        <div className="actions">
          <button onClick={onEdit}>
            <FontAwesomeIcon icon={faPencilAlt} />
          </button>
          <button onClick={onDelete}>
            <FontAwesomeIcon icon={faTrashAlt} />
          </button>
        </div>
      )}
    </Link>
  );
};
