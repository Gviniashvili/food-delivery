import React, { useContext, useEffect, useState } from 'react';
import { SettingsStore } from '../../stores/Settings';
import { RestaurantsApi as AdminRestaurantsApi } from '../../api/Admin/Restaurants';
import { Restaurants as RestaurantsApi } from '../../api/Restaurants';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { RestaurantContainer } from './Controller';

export const Restaurants = ({ editMode }) => {
  const settingsStore = useContext(SettingsStore);

  const [restaurants, setRestaurants] = useState([]);
  const API = editMode ? AdminRestaurantsApi : RestaurantsApi;

  useEffect(function () {
    API.index(settingsStore.state.user.access_token)
      .then((list) => {
        if (list && list.length) setRestaurants(list);
      })
      .catch(() => console.error('Unable to load restaurants'));
  }, []);

  const addNewRestaurant = () => {
    if (restaurants[0].id) {
      // If we don't have any unadded item
      setRestaurants([{ id: null, name: '', description: '' }, ...restaurants]);
    }
  };

  const handleRestaurantEdit = async (id, newValues) => {
    if (!id) {
      const result = await AdminRestaurantsApi.create(
        { ...newValues },
        settingsStore.state.user.access_token
      );
      setRestaurants([result.restaurant, ...restaurants.slice(1)]);
      return { finishEditing: result.success };
    } else {
      const result = await AdminRestaurantsApi.update(
        { id, ...newValues },
        settingsStore.state.user.access_token
      );
      return { finishEditing: result.success };
    }
  };

  const handleRestaurantDelete = async (id) => {
    await AdminRestaurantsApi.remove(id, settingsStore.state.user.access_token);
    const newList = restaurants.filter((r) => r.id !== id);
    setRestaurants([...newList]);
  };

  return (
    <>
      <div className="page-heading">
        <h1 className="mb-2 mb-md-0">Our Restaurants</h1>
        {editMode && <button className="btn btn-primary" onClick={addNewRestaurant}>
          <FontAwesomeIcon icon={faPlus} /> Add a Restaurant
        </button>}
      </div>
      <div className="row mb-4">
        {restaurants.map((item) => (
          <div
            className="col-sm-6 col-md-4 mt-4"
            key={item.id || Math.random()}
          >
            <RestaurantContainer
              editMode={editMode}
              id={item.id}
              isNew={!item.id}
              name={item.name}
              description={item.description}
              onEdit={(newValues) => handleRestaurantEdit(item.id, newValues)}
              onDelete={() => handleRestaurantDelete(item.id)}
            />
          </div>
        ))}
      </div>
    </>
  );
};
