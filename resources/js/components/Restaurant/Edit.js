import React, { useRef, useEffect } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSave } from '@fortawesome/free-solid-svg-icons';

export const Edit = ({
  name,
  description,
  error,
  isLoading,
  onCancelEdit,
  onConfirmEdit,
  onNameChange,
  onDescriptionChange,
}) => {
  const inputRef = useRef(null);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const onSubmit = (e) => {
    e.preventDefault();
    onConfirmEdit();
  }

  return (
    <form className="restaurant" onSubmit={onSubmit}>
      <div className="heading">
        <input
          ref={inputRef}
          type="text"
          className="form-control"
          defaultValue={name}
          disabled={isLoading}
          required
          onChange={(e) => onNameChange(e.target.value)}
        />
      </div>
      <textarea
        rows="4"
        className="form-control"
        defaultValue={description}
        disabled={isLoading}
        required
        onChange={(e) => onDescriptionChange(e.target.value)}
      />
      <div className="footer">
        <button
          type="submit"
          className="btn btn-primary"
          disabled={isLoading}
        >
          <FontAwesomeIcon icon={faSave} /> Save
        </button>
        <button
          className="btn btn-default"
          onClick={onCancelEdit}
          disabled={isLoading}
        >
          Cancel
        </button>
      </div>

      {error && <div className="text-danger">{error}</div>}
    </form>
  );
};
