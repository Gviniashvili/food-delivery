import React, { useState } from 'react';
import { View as RestaurantView } from './View';
import ConfirmDialog from '../ConfirmDialog';
import { Edit as RestaurantEdit } from './Edit';

export const RestaurantContainer = ({
  editMode,
  id,
  name,
  description,
  isNew,
  error,
  onEdit,
  onDelete,
}) => {
  const [_name, setName] = useState(name);
  const [_description, setDescription] = useState(description);
  const [isEditing, setIsEditing] = useState(isNew);
  const [isLoading, setIsLoading] = useState(false);
  const [confirmOpen, setConfirmOpen] = useState(false);

  const handleConfirmEdit = () => {
    setIsLoading(true);
    onEdit({ name: _name, description: _description })
      .then(({ finishEditing }) => {
        setIsEditing(!finishEditing);
        setIsLoading(false);
      })
      .catch((err) => {
        setIsLoading(false);
        throw err;
      });
  };

  const onStartEdit = (e) => {
    setIsEditing(true);
    e.stopPropagation();
    e.preventDefault();
  }

  const onStartDelete = (e) => {
    setConfirmOpen(true);
    e.stopPropagation();
    e.preventDefault();
  }

  const onCancelEdit = (e) => {
    setIsEditing(false);
    if(isNew) onDelete();
  }

  return (
    <>
      {editMode && isEditing ? (
        <RestaurantEdit
          name={_name}
          description={_description}
          error={error}
          isEditing={isEditing}
          isLoading={isLoading}
          onCancelEdit={onCancelEdit}
          onNameChange={(name) => setName(name)}
          onDescriptionChange={(description) => setDescription(description)}
          onConfirmEdit={handleConfirmEdit}
        />
      ) : (
        <RestaurantView
          editMode={editMode}
          addToCart={!editMode}
          id={id}
          name={_name}
          description={_description}
          onEdit={onStartEdit}
          onDelete={onStartDelete}
        />
      )}

      <ConfirmDialog
        title={'Deleting ' + _name}
        open={confirmOpen}
        setOpen={setConfirmOpen}
        onConfirm={onDelete}
        yesBtnText={'Delete'}
      >
        Are you sure you want to delete {_name} restaurant?
      </ConfirmDialog>
    </>
  );
};
