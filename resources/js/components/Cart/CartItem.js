import React from 'react';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export const CartItem = ({ item, onRemove, onChangeQuantity }) => {
  return (
    <div className="cart-item">
      <div className="heading">
        <h2>{item.name}</h2>
        <button onClick={onRemove} title={'Remove'}>
          <FontAwesomeIcon icon={faTimes} />
        </button>
      </div>

      <div className="quantity">
        <span>Quantity:</span>
        <button
          disabled={item.quantity === 1}
          onClick={() => onChangeQuantity(item.quantity - 1)}
        >
          -
        </button>
        <div className="value">{item.quantity}</div>
        <button onClick={() => onChangeQuantity(item.quantity + 1)}>+</button>
      </div>
    </div>
  );
};
