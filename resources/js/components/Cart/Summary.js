import React from "react";
import {Pricing} from "./Pricing";
import {getTotalPrice} from "../../helpers/getTotalPrice";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const Summary = ({ title = 'Your order', items, onRemoveItem, onCreateOrder, withAction = true }) => {
  const formattedTotalPrice = getTotalPrice(items, true);

  return (
    <div className="cart-summary">
      <div className="heading">
        <h4>{title}</h4>
        <FontAwesomeIcon icon={faShoppingCart} />
      </div>

      {items.length ? <>
        <Pricing items={items} formattedTotalPrice={formattedTotalPrice} onRemoveItem={onRemoveItem} withAction={withAction} />

        {withAction && <div className="action">
          <button className="btn btn-primary" onClick={onCreateOrder}>ORDER NOW</button>
        </div>}
      </> : <div className="empty">
        Cart is empty
      </div>}
    </div>
  );
}