import React from 'react';
import { formatPrice } from '../../helpers/formatPrice';
import {faMinusCircle} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export const Pricing = ({ items, formattedTotalPrice, withAction, withoutTotalPrice, onRemoveItem }) => {
  return (
    <dl className="pricing">
      {items.map((meal) => (
        <div key={meal.id} className="item">
          <dt>
            {withAction && <button className="remove" onClick={() => onRemoveItem(meal.id)}><FontAwesomeIcon icon={faMinusCircle} /></button>}
            <span className="quantity">{meal.quantity}x</span>
            <div>{meal.name}</div>
          </dt>
          <dd>{formatPrice(meal.price, meal.quantity)}</dd>
        </div>
      ))}

      {!withoutTotalPrice && <div className="item total">
        <dt>Total</dt>
        <dd>{formattedTotalPrice}</dd>
      </div>}
    </dl>
  );
};
