import React, { useContext } from 'react';
import { SettingsStore } from '../stores/Settings';
import { LoginView } from '../views/LoginView';
import { AdminView } from '../views/AdminView';
import { RegularUserView } from '../views/RegularUserView';

function Content() {
  const settingsStore = useContext(SettingsStore);

  return (
    <main>
      <div className="container">
        {settingsStore.state.user && settingsStore.state.user.is_admin ? (
          <AdminView />
        ) : settingsStore.state.user ? (
          <RegularUserView />
        ) : (
          <LoginView />
        )}
      </div>
    </main>
  );
}

export default Content;
