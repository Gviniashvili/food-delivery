import React from 'react';
import ReactDOM from 'react-dom';

function Footer() {
  return (
    <footer>
      <div className="container">
        <div>All rights reserved &copy; {new Date().getFullYear()}</div>
      </div>
    </footer>
  );
}

export default Footer;
