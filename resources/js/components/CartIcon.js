import React, { useContext } from "react";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faShoppingCart} from "@fortawesome/free-solid-svg-icons";
import {SettingsStore} from "../stores/Settings";
import {Link} from "react-router-dom";

export const CartIcon = () => {
  const settingsStore = useContext(SettingsStore);

  return (
    <Link to={'/cart'} className="cart-icon">
      <FontAwesomeIcon icon={faShoppingCart} className="mr-2" />
      <span>{settingsStore.state.cart?.length || 0}</span>
    </Link>
  );
}