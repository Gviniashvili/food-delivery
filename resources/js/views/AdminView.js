import React from 'react';
import { Restaurants } from '../components/Restaurant/index';
import { Meals } from '../components/Meal/index';
import { Switch, Route } from 'react-router-dom';
import {AdminOrders} from "../components/AdminOrders";

export const AdminView = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Restaurants editMode={true} />
      </Route>
      <Route path="/restaurant/:id">
        <Meals editMode={true} />
      </Route>
      <Route path="/orders">
        <AdminOrders />
      </Route>
    </Switch>
  );
};
