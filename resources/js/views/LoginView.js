import React, { useContext, useState } from 'react';
import AuthApi from '../api/Auth';
import { SettingsStore } from '../stores/Settings';

const formData = {
  name: '',
  email: '',
  password: '',
  password_confirmation: '',
};

export const LoginView = () => {
  const settingsStore = useContext(SettingsStore);

  const [mode, setMode] = useState('login');
  const [errors, setErrors] = useState(null);

  function submit(e) {
    if (mode === 'register') {
      AuthApi.register(formData).then(onLogin).catch(processResponse);
    } else {
      AuthApi.login(formData)
        .then(onLogin)
        .catch((err) => {
          if (err.errors) {
            setErrors(err.errors);
          } else {
            setErrors({ general: 'Invalid credentials' });
          }
        });
    }

    e.preventDefault();
  }

  const onLogin = (r) => {
    if (r && r.user && r.access_token) {
      settingsStore.dispatch({
        type: 'UPDATE',
        user: { ...r.user, access_token: r.access_token },
      });
    } else {
      processResponse(r);
    }
  };

  const processResponse = (res) => {
    if (res.errors) {
      setErrors(res.errors);
    } else if (res.error) {
      setErrors({ general: res.error });
    } else {
      setErrors({ general: 'An error occurred, please try again later' });
    }
  };

  if (mode === 'register') {
    return (
      <form className="login-form" onSubmit={submit}>
        <div className="form-group">
          <label htmlFor="name" className="control-label">
            Full name:
          </label>
          <input
            type="text"
            id="name"
            className="form-control"
            name="name"
            onChange={({ target: { value: v } }) => (formData.name = v)}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="email" className="control-label">
            Email:
          </label>
          <input
            type="email"
            id="email"
            className="form-control"
            name="email"
            onChange={({ target: { value: v } }) => (formData.email = v)}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="password" className="control-label">
            Password:
          </label>
          <input
            type="password"
            id="password"
            className="form-control"
            name="password"
            onChange={({ target: { value: v } }) => (formData.password = v)}
            required
          />
        </div>

        <div className="form-group">
          <label htmlFor="password_confirmation" className="control-label">
            Confirm password:
          </label>
          <input
            type="password"
            id="password_confirmation"
            className="form-control"
            name="password_confirmation"
            onChange={({ target: { value: v } }) => (formData.password_confirmation = v)}
            required
          />
        </div>

        <div className="form-group">
          <div className="row">
            <div className="col-sm-6 d-flex align-items-center">
              <a
                onClick={() => {
                  setMode('login');
                  setErrors(null);
                }}
              >
                Sign in
              </a>
            </div>

            <div className="col-sm-6 text-right">
              <button className="btn btn-primary" type="submit">
                Register
              </button>
            </div>
          </div>
        </div>

        {errors ? (
          <div className="form-group">
            <ul>
              {Object.keys(errors).map((k) => (
                <li key={k} className="text-danger">
                  {errors[k]}
                </li>
              ))}
            </ul>
          </div>
        ) : null}
      </form>
    );
  }

  return (
    <form className="login-form" onSubmit={submit}>
      <div className="form-group">
        <label htmlFor="email" className="control-label">
          Email:
        </label>
        <input
          className="form-control"
          type="email"
          name="email"
          onChange={({ target: { value: v } }) => (formData.email = v)}
          required
        />
      </div>

      <div className="form-group">
        <label htmlFor="password" className="control-label">
          Password:
        </label>
        <input
          className="form-control"
          type="password"
          name="password"
          onChange={({ target: { value: v } }) => (formData.password = v)}
          required
        />
      </div>

      <div className="form-group">
        <div className="row">
          <div className="col-sm-6 d-flex align-items-center">
            <a
              onClick={() => {
                setMode('register');
                setErrors(null);
              }}
            >
              Sign up
            </a>
          </div>

          <div className="col-sm-6 text-right">
            <button className="btn btn-primary" type="submit">
              Login
            </button>
          </div>
        </div>
      </div>

      {errors ? (
        <div className="form-group">
          <ul>
            {Object.keys(errors).map((k) => (
              <li key={k} className="text-danger">
                {errors[k]}
              </li>
            ))}
          </ul>
        </div>
      ) : null}
    </form>
  );
};
