import React, { useContext } from 'react';
import { SettingsStore } from '../stores/Settings';
import { Restaurants } from '../components/Restaurant';
import { Meals } from '../components/Meal';
import { Orders } from '../components/Orders';
import { Switch, Route } from 'react-router-dom';

export const RegularUserView = () => {
  return (
    <Switch>
      <Route exact path="/">
        <Restaurants editMode={false} />
      </Route>
      <Route path="/restaurant/:id">
        <Meals editMode={false} />
      </Route>
      <Route path="/orders">
        <Orders />
      </Route>
    </Switch>
  );
};
