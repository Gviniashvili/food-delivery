import React from 'react'
import request from './Request'

async function index(access_token) {
  return await request({
    url: '/api/restaurants',
    access_token: access_token
  });
}

export const Restaurants = {
  index: index,
}