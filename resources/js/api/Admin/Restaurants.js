import React from 'react'
import request from '../Request'

async function index(access_token) {
  return await request({
    url: '/api/admin/restaurants',
    access_token: access_token
  });
}

async function create(data, access_token) {
  return await request({
    url: '/api/admin/restaurants',
    method: 'POST',
    data: data,
    access_token: access_token
  });
}

async function update(data, access_token) {
  return await request({
    url: '/api/admin/restaurants/' + data.id,
    method: 'PUT',
    data: data,
    access_token: access_token
  });
}

async function remove(id, access_token) {
  return await request({
    url: '/api/admin/restaurants/' + id,
    method: 'DELETE',
    data: id,
    access_token: access_token
  });
}

export const RestaurantsApi = {
  index: index,
  create: create,
  update: update,
  remove: remove,
}