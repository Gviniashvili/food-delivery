import React from 'react'
import request from '../Request'

async function index(restaurantId, access_token) {
  return await request({
    url: '/api/admin/meals?restaurant_id=' + restaurantId,
    access_token: access_token
  });
}

async function create(data, access_token) {
  return await request({
    url: '/api/admin/meals',
    method: 'POST',
    data: data,
    access_token: access_token
  });
}

async function update(data, access_token) {
  return await request({
    url: '/api/admin/meals/' + data.id,
    method: 'PUT',
    data: data,
    access_token: access_token
  });
}

async function remove(id, access_token) {
  return await request({
    url: '/api/admin/meals/' + id,
    method: 'DELETE',
    data: id,
    access_token: access_token
  });
}

export const MealsApi = {
  index: index,
  create: create,
  update: update,
  remove: remove,
}