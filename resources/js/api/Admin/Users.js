import React from 'react'
import request from '../Request'

async function block(userId, access_token) {
  return await request({
    url: '/api/admin/users/' + userId + '/block',
    method: 'PUT',
    access_token: access_token
  });
}

async function unblock(userId, access_token) {
  return await request({
    url: '/api/admin/users/' + userId + '/unblock',
    method: 'PUT',
    access_token: access_token
  });
}

export const UsersApi = {
  block: block,
  unblock: unblock,
}