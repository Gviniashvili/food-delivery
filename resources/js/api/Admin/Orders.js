import React from 'react'
import request from '../Request'

async function index(access_token) {
  return await request({
    url: '/api/admin/orders',
    access_token: access_token
  });
}

async function updateStatus(orderId, status, access_token) {
  return await request({
    url: '/api/admin/orders/' + orderId,
    method: 'PUT',
    data: { status },
    access_token: access_token
  });
}

export const OrdersApi = {
  index: index,
  updateStatus: updateStatus,
}