import React from 'react'
import request from './Request'

async function index(restaurantId, access_token) {
  return await request({
    url: '/api/meals?restaurant_id=' + restaurantId,
    access_token: access_token
  });
}

export const Meals = {
  index: index,
}