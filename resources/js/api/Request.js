import React from 'react'

async function request(cfg) {
  if(!cfg.url) throw "[API Request] Url parameter is not present";

  const controller = new AbortController();
  const { signal } = controller;

  if(cfg.controller) cfg.controller(controller);

  const params = {
    method: cfg.method || 'GET',
    headers: {
      'Accept':           'application/json',
      'Content-Type':     'application/json',
      'X-Requested-With': 'XMLHttpRequest',
    },
    body: JSON.stringify(cfg.data),
    signal: signal,
  };

  if(cfg.access_token) params.headers['Authorization'] = 'Bearer ' + cfg.access_token;

  const sendRequest = async () => {
    const response = await fetch(cfg.url, params);

    if(response.status === 401) { // Unauthenticated
      localStorage.setItem('settings', null);
      window.location.reload();
    }

    return await response.json();
  }

  return await sendRequest();
}

export default request