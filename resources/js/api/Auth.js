import React from 'react'
import request from './Request'

async function login(credentials) {
  return await request({
    url: '/api/login',
    method: 'POST',
    data: credentials
  });
}

async function logout(access_token) {
  return await request({
    url: '/api/logout',
    method: 'POST',
    access_token: access_token,
  });
}

async function register(props) {
  return await request({
    url: '/api/register',
    method: 'POST',
    data: props
  });
}

export default {
  login: login,
  logout: logout,
  register: register,
}