import React from 'react'
import request from './Request'

async function index(access_token) {
  return await request({
    url: '/api/orders',
    access_token: access_token
  });
}

async function create(data, access_token) {
  return await request({
    url: '/api/orders',
    method: 'POST',
    data: data,
    access_token: access_token
  });
}

async function updateStatus(orderId, status, access_token) {
  return await request({
    url: '/api/orders/' + orderId,
    method: 'PUT',
    data: { status },
    access_token: access_token
  });
}

export const OrdersApi = {
  index: index,
  create: create,
  updateStatus: updateStatus,
}