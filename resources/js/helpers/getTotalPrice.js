import React from "react";
import {formatPrice} from "./formatPrice";

export const getTotalPrice = (items, formatted = false) => {
  let totalPrice = 0;
  items.map((item) => totalPrice += item.price * item.quantity);

  return formatted ? formatPrice(totalPrice) : totalPrice;
}