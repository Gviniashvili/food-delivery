import React from "react";

export const formatPrice = (price, quantity = 1) => {
  return '$' + (price * quantity / 100).toFixed(2);
}