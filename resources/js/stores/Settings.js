import React, {createContext, useReducer, useEffect} from 'react';

let initialState = { loaded: false, user: null };
const SettingsStore = createContext(initialState);

const SettingsProvider = ({ children }) => {
  const [state, dispatch] = useReducer((state, action) => {
    const s = {...state};
    //console.log('action', action);

    switch (action.type) {
      case 'INIT':
        action.loaded = true;
        return action;
      case 'UPDATE':
        for(let p in action) {
          if(action.hasOwnProperty(p) && p !== 'type') s[p] = action[p];
        }
        localStorage.setItem('settings', JSON.stringify(s));
        return s;
      default:
        throw "[SettingsStore] Unknown action received in the reducer";
    }
  }, initialState);

  useEffect(function () {
    try {
      let settings = localStorage.getItem('settings');
      if(settings) settings = JSON.parse(settings);
      if(settings) dispatch({ type: 'INIT', ...settings });
      else dispatch({ type: 'INIT', ...initialState });
    } catch(e) {
      dispatch({ type: 'INIT', ...initialState });
    }
  }, []);

  return <SettingsStore.Provider value={{ state, dispatch }}>{children}</SettingsStore.Provider>;
};

export { SettingsStore, SettingsProvider }