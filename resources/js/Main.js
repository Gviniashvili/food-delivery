import React from 'react';
import ReactDOM from 'react-dom';
import Header from "./components/Header";
import Content from "./components/Content";
import Footer from "./components/Footer";
import {SettingsProvider} from "./stores/Settings";
import { BrowserRouter as Router } from "react-router-dom";

function Main() {
  return (
    <Router>
      <SettingsProvider>
        <Header />
        <Content />
        <Footer />
      </SettingsProvider>
    </Router>
  );
}

export default Main;

if (document.getElementById('root')) {
  ReactDOM.render(<Main/>, document.getElementById('root'));
}
