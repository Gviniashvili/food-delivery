<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Laravel</title>
    <link rel="stylesheet" href="/css/app.css">
    <base href="/">
</head>
<body>
<div id="root"></div>
<script src="/js/app.js"></script>
</body>
</html>
