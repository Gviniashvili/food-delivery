## About Food Delivery

This is a test application created for Toptal screening process

## Installation

### Server requirements

This application is written on PHP Laravel framework v8
In order to run Laravel 8 you will need to make sure your server meets the following requirements:
- PHP >= 7.2.5
- BCMath PHP Extension
- Ctype PHP Extension
- Fileinfo PHP extension
- JSON PHP Extension
- Mbstring PHP Extension
- OpenSSL PHP Extension
- PDO PHP Extension
- Tokenizer PHP Extension
- XML PHP Extension

### How to run

In order to run this application, run following commands in terminal:

- `./composer.phar install`
- `npm install`
- `cp .env.example .env` (Configure your database credentials in the .env file)
- `php artisan key:generate`
- `php artisan migrate:fresh --seed`
- `php artisan passport:install`
- `npm run dev`
- `php artisan serve` (Open the given URL in your browser)

## Test Accounts

To login as admin use following credentials:
- admin@fooddelivery.ge
- 123456

To login as regular user you can create a new account or use following credentials:
- user@fooddelivery.ge
- 123456

## Testing

Application is using phpunit tests provided by Laravel.
In order to run tests you need to copy `.env` file to `.env.testing` and set up a new database configuration for testing.

### Running tests
- `php artisan test`