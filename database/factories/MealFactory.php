<?php

namespace Database\Factories;

use App\Models\Meal;
use App\Models\Restaurant;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class MealFactory extends Factory {
  /**
   * The name of the factory's corresponding model.
   *
   * @var string
   */
  protected $model = Meal::class;

  private $foodNames = [
    'Apples', 'Avocado', 'Baked beans', 'Beer',
    'Chicken', 'Cheese', 'Cupcakes', 'Eggs',
    'Falafel', 'Ginger', 'Hummus', 'Ice cream',
    'Lamb', 'Noodles', 'Ostrich', 'Spinach',
    'Toast', 'Wine', 'Waffles', 'Yogurt',
  ];

  /**
   * Define the model's default state.
   *
   * @return array
   */
  public function definition() {
    return [
      'name' => $this->foodNames[rand(0, count($this->foodNames) - 1)],
      'description' => $this->faker->text(100),
      'price' => $this->faker->numberBetween(10, 1000) * 10,
      'restaurant_id' => Restaurant::inRandomOrder()->first()->id,
    ];
  }
}
