<?php

use App\Models\Order;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('orders', function (Blueprint $table) {
      $table->id();
      $table->timestamps();

      $table->json('data');
      $table->integer('total_amount');
      $table->string('status')->default(Order::PLACED);

      $table->unsignedBigInteger('restaurant_id')->nullable();
      $table->foreign('restaurant_id')->references('id')->on('restaurants')->onDelete('set null');

      $table->unsignedBigInteger('user_id')->nullable();
      $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
    });

    Schema::create('order_status_updates', function (Blueprint $table) {
      $table->id();
      $table->timestamp('created_at');
      $table->string('status');

      $table->unsignedBigInteger('order_id');
      $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');

      $table->unsignedBigInteger('user_id');
      $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::dropIfExists('order_status_updates');
    Schema::dropIfExists('orders');
  }
}
