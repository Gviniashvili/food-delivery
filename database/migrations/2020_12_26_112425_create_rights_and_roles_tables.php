<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

/**
 * Class CreateRightsAndRolesTable
 */
class CreateRightsAndRolesTables extends Migration {
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up() {
    Schema::create('rights', function (Blueprint $table) {
      $table->increments('id');
      $table->string('right_key');
      $table->text('right_description');
      $table->timestamps();
    });

    Schema::create('roles', function (Blueprint $table) {
      $table->increments('id');
      $table->string('role_key')->nullable()->default(null);
      $table->unique('role_key');
      $table->text('role_description');
      $table->timestamps();
    });

    Schema::create('role_rights', function (Blueprint $table) {
      $table->unsignedInteger('role_id');
      $table->unsignedInteger('right_id');
      $table->primary(['role_id', 'right_id']);
      $table->timestamps();

      $table->foreign('role_id')->references('id')->on('roles')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('right_id')->references('id')->on('rights')
        ->onUpdate('cascade')->onDelete('cascade');
    });

    Schema::create('user_roles', function (Blueprint $table) {
      $table->unsignedBigInteger('user_id');
      $table->unsignedInteger('role_id');
      $table->primary(['user_id', 'role_id']);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('role_id')->references('id')->on('roles')
        ->onUpdate('cascade')->onDelete('cascade');
    });

    Schema::create('user_rights', function (Blueprint $table) {
      $table->unsignedBigInteger('user_id');
      $table->unsignedInteger('right_id');
      $table->primary(['user_id', 'right_id']);
      $table->timestamps();

      $table->foreign('user_id')->references('id')->on('users')
        ->onUpdate('cascade')->onDelete('cascade');

      $table->foreign('right_id')->references('id')->on('rights')
        ->onUpdate('cascade')->onDelete('cascade');
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down() {
    Schema::drop('user_rights');
    Schema::drop('user_roles');
    Schema::drop('role_rights');
    Schema::drop('roles');
    Schema::drop('rights');
  }
}
