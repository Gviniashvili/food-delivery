<?php


namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class RightsAndRoles extends Seeder {
  /**
   * @var array
   */
  private $data = [
    'rights' => [
      /* Brands */
      ['right_key' => 'restaurants.manage', 'right_description' => 'Manage restaurants']
    ],

    'roles' => [
      [
        'role_key' => 'restaurant_owner',
        'role_description' => 'Restaurant owner',
        '__rights_keys__' => ['restaurants.manage'],
      ],
    ],

  ];

  /**
   *
   */
  public function run() {
    $rightKeyIdMap = [];
    foreach ($this->data['rights'] as $right) {
      $rightId = \DB::table('rights')->insertGetId(
        array_merge($right, [
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ])
      );
      $rightKeyIdMap[$right['right_key']] = $rightId;
    }

    foreach ($this->data['roles'] as $role) {
      $roleAttributes = Arr::only($role, ['role_key', 'role_description']);
      $roleId = \DB::table('roles')->insertGetId(
        array_merge($roleAttributes, [
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ])
      );

      foreach ($role['__rights_keys__'] as $roleRightKey) {
        $rightId = $rightKeyIdMap[$roleRightKey];
        \DB::table('role_rights')->insert([
          'role_id' => $roleId,
          'right_id' => $rightId,
          'created_at' => \Carbon\Carbon::now(),
          'updated_at' => \Carbon\Carbon::now(),
        ]);
      }
    }
  }
}