<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $user = new User([
      'name' => 'Admin User',
      'email' => 'admin@fooddelivery.ge',
      'password' => bcrypt('123456'),
      'remember_token' => str_random(32)
    ]);
    $user->save();

    $managerRole = Role::whereRoleKey('restaurant_owner')->first();
    $user->roles()->attach($managerRole); // Attach restaurant manager role

    $user = new User([
      'name' => 'Regular User',
      'email' => 'user@fooddelivery.ge',
      'password' => bcrypt('123456'),
      'remember_token' => str_random(32)
    ]);
    $user->save();
  }
}