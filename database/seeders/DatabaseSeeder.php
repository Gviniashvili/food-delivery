<?php

namespace Database\Seeders;

use App\Models\Meal;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
  /**
   * Seed the application's database.
   *
   * @return void
   */
  public function run() {
    $this->call([
      RightsAndRoles::class,
      UserSeeder::class,
      RestaurantSeeder::class,
    ]);

    Meal::factory(100)->create();
  }
}
