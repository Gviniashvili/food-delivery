<?php

namespace Database\Seeders;

use App\Models\Restaurant;
use Illuminate\Database\Seeder;

class RestaurantSeeder extends Seeder {
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run() {
    $restaurants = [
      [ 'name' => 'McDonald\'s', 'description' => 'Fast food' ],
      [ 'name' => 'Subway', 'description' => 'Fast & Healthy Sub Sandwiches' ],
      [ 'name' => 'Panda Express', 'description' => 'American Chinese food fresh from the wok' ],
      [ 'name' => 'Starbucks', 'description' => 'More than just great coffee' ],
      [ 'name' => 'Wendy\'s', 'description' => 'Fresh-made food with drive-thru or delivery' ],
      [ 'name' => 'Pizza Hut', 'description' => 'Order pizza online from a store near you' ],
    ];

    foreach($restaurants as $restaurant) {
      $restaurant = new Restaurant([
        'name' => $restaurant['name'],
        'description' => $restaurant['description'],
      ]);
      $restaurant->save();
    }
  }
}
