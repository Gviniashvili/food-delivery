<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Meal;
use App\Models\Order;
use App\Models\OrderStatusUpdate;
use App\Models\Restaurant;
use App\Services\OrderService;

class OrderController extends Controller {
  private $orderService;

  function __construct(OrderService $orderService) {
    $this->orderService = $orderService;
  }

  public function index() {
    $user = auth()->user();

    $orders = $user->orders()->orderBy('created_at', 'DESC')->get();

    return $orders->map(function ($order) {
      return $order->toCustomJson();
    });
  }

  public function store() {
    try {
      $user = auth()->user();

      $orderItems = collect(request('meals') ?? []);
      if (!$orderItems) throw new \Exception("Please select meals to order");

      $order = $this->orderService->createOrder($orderItems, $user);

      return ['order' => $order->toCustomJson()];
    } catch(\Exception $e) {
      return ['error' => $e->getMessage()];
    }
  }

  public function updateStatus(Order $order) {
    $user = auth()->user();
    $status = request('status');

    if($status == 'cancelled') {
      if ($order->status != Order::PLACED) {
        throw new \Exception("Order is already processed");
      }

      $this->orderService->updateOrderStatus($order, Order::CANCELLED, $user);
    } else if($status == 'received') {
      if ($order->status != Order::DELIVERED) {
        throw new \Exception("Order is not yet delivered");
      }

      $this->orderService->updateOrderStatus($order, Order::RECEIVED, $user);
    } else {
      throw new \Exception("Unknown status");
    }

    return ['order' => $order->toCustomJson()];
  }
}