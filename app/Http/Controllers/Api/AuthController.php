<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegistrationRequest;
use App\Models\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class AuthController extends Controller {
  use ThrottlesLogins;

  private function username() {
    return 'email';
  }

  public function login(LoginRequest $request) {
    try {
      if ($this->hasTooManyLoginAttempts($request)) {
        $this->fireLockoutEvent($request);
        $this->sendLockoutResponse($request);
      }

      $credentials = [
        'email' => request('email'),
        'password' => request('password'),
      ];

      if(Auth::attempt($credentials, true)) {
        $this->clearLoginAttempts($request);
        $user = User::where('email', $request->input('email'))->first();
        if($user->blocked_at) throw new \Exception('Your account is suspended');

        $scopes = [];
        if($user->isAdmin()) $scopes[] = 'manage-restaurants';

        return [
          'user' => $user->toCustomJson(),
          'access_token' => $user->createToken('webApp', $scopes)->accessToken,
        ];
      } else {
        $this->incrementLoginAttempts($request);
        throw new \Exception('Invalid credentials');
      }
    } catch(\Exception $e) {
      return [
        'error' => $e->getMessage()
      ];
    }
  }

  public function logOut(Request $request) {
    try {
      $request->user()->token()->revoke();
    } catch(\Exception $e) {
      // Do nothing
    }

    return [ 'success' => true ];
  }

  public function register(RegistrationRequest $request)
  {
    $name = trim(strip_tags($request->input('name')));

    $user = new User([
      'name' => $name,
      'email' => $request->input('email'),
      'password' => bcrypt($request->input('password')),
      'remember_token' => Str::random(32),
    ]);
    $user->save();

    return [
      'user' => $user->toCustomJson(),
      'access_token' => $user->createToken('webApp')->accessToken,
    ];
  }
}