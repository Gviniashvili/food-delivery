<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\User;
use Laravel\Passport\TokenRepository;

class UserController extends Controller {
  public function block(User $user) {
    $user->fill([ 'blocked_at' => date('Y-m-d H:i:s') ])->save();

    $tokenRepository = app(TokenRepository::class);

    foreach($user->tokens as $token) {
      $tokenRepository->revokeAccessToken($token->id);
    }

    return [
      'user' => $user->toCustomJson()
    ];
  }

  public function unblock(User $user) {
    $user->fill([ 'blocked_at' => null ])->save();

    return [
      'user' => $user->toCustomJson()
    ];
  }
}