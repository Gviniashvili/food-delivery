<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateMeal;
use App\Models\Meal;
use App\Models\Restaurant;

class MealController extends Controller
{
  public function index()
  {
    $meals = Meal::whereRestaurantId(request('restaurant_id'))->orderBy('id', 'DESC')->get();

    return [
      'restaurant' => Restaurant::findOrFail(request('restaurant_id')),
      'meals' => $meals->map(function($item) { return $item->toCustomJson(); }),
    ];
  }

  public function store(CreateMeal $request)
  {
    $meal = new Meal([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
      'price' => $request->input('price'),
    ]);
    $meal->restaurant()->associate($request->input('restaurant_id'));
    $meal->save();

    return [ 'success' => true, 'meal' => $meal->toCustomJson() ];
  }

  public function update(CreateMeal $request, Meal $meal)
  {
    $meal->fill([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
      'price' => $request->input('price'),
    ]);
    $meal->save();

    return [ 'success' => true, 'meal' => $meal->toCustomJson() ];
  }

  public function destroy(Meal $meal)
  {
    $meal->delete();

    return [ 'success' => true ];
  }
}
