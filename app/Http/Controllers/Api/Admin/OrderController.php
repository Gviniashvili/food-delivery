<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Models\Meal;
use App\Models\Order;
use App\Models\OrderStatusUpdate;
use App\Models\Restaurant;
use App\Services\OrderService;

class OrderController extends Controller {
  private $orderService;

  function __construct(OrderService $orderService) {
    $this->orderService = $orderService;
  }

  public function index() {
    $orders = Order::orderBy('created_at', 'DESC')->get();

    return $orders->map(function ($order) {
      return $order->toCustomJson();
    });
  }

  public function updateStatus(Order $order) {
    $user = auth()->user();

    $status = request('status');

    if($status == 'processing') {
      if ($order->status != Order::PLACED) {
        throw new \Exception("You can mark as delivered orders with placed status");
      }

      $this->orderService->updateOrderStatus($order, Order::PROCESSING, $user);
    } else if($status == 'in_route') {
      if ($order->status != Order::PROCESSING) {
        throw new \Exception("You can mark as delivered orders with processing status");
      }

      $this->orderService->updateOrderStatus($order, Order::IN_ROUTE, $user);
    } else if($status == 'delivered') {
      if ($order->status != Order::IN_ROUTE) {
        throw new \Exception("You can mark as delivered orders with in route status");
      }

      $this->orderService->updateOrderStatus($order, Order::DELIVERED, $user);
    } else {
      throw new \Exception("Unknown status");
    }

    return ['order' => $order->toCustomJson()];
  }
}