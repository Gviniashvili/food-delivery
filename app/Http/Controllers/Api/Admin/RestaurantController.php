<?php

namespace App\Http\Controllers\Api\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CreateRestaurant;
use App\Models\Restaurant;
use App\Models\Right;

class RestaurantController extends Controller
{
  public function index()
  {
    $restaurants = Restaurant::orderBy('id', 'DESC')->get();

    return $restaurants->map(function($item) { return $item->toCustomJson(); });
  }

  public function store(CreateRestaurant $request)
  {
    $restaurant = new Restaurant([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
    ]);
    $restaurant->save();

    return [ 'success' => true, 'restaurant' => $restaurant->toCustomJson() ];
  }

  public function update(CreateRestaurant $request, Restaurant $restaurant)
  {
    $restaurant->fill([
      'name' => $request->input('name'),
      'description' => $request->input('description'),
    ]);
    $restaurant->save();

    return [ 'success' => true, 'restaurant' => $restaurant->toCustomJson() ];
  }

  public function destroy(Restaurant $restaurant)
  {
    $restaurant->delete();

    return [ 'success' => true ];
  }
}
