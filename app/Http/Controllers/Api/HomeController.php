<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Meal;
use App\Models\Restaurant;

class HomeController extends Controller {
  public function restaurants() {
    $restaurants = Restaurant::orderBy('id', 'DESC')->get();

    return $restaurants->map(function ($item) {
      return $item->toCustomJson();
    });
  }

  public function meals() {
    $meals = Meal::whereRestaurantId(request('restaurant_id'))->orderBy('id', 'DESC')->get();

    return [
      'restaurant' => Restaurant::findOrFail(request('restaurant_id')),
      'meals' => $meals->map(function ($item) {
        return $item->toCustomJson();
      }),
    ];
  }
}