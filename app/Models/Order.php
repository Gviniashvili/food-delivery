<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model {
  use HasFactory;

  const PLACED = 'placed';
  const CANCELLED = 'cancelled';
  const PROCESSING = 'processing';
  const IN_ROUTE = 'in_route';
  const DELIVERED = 'delivered';
  const RECEIVED = 'received';

  protected $casts = [
    'data' => 'object',
  ];

  protected $fillable = ['data', 'total_amount', 'status'];

  public function restaurant() {
    return $this->belongsTo(Restaurant::class, 'restaurant_id');
  }

  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function statusUpdates() {
    return $this->hasMany(OrderStatusUpdate::class, 'order_id');
  }

  public function toCustomJson() {
    $actions = [];

    $user = auth()->user();

    if($user->isAdmin()) {
      if($this->status === Order::PLACED) $actions[] = 'processing';
      if($this->status === Order::PROCESSING) $actions[] = 'in_route';
      if($this->status === Order::IN_ROUTE) $actions[] = 'delivered';
    } else {
      if($this->status === Order::PLACED) $actions[] = 'cancel';
      if($this->status === Order::DELIVERED) $actions[] = 'received';
    }

    return [
      'id' => $this->id,
      'date' => $this->created_at->format('F d, Y / H:i'),
      'data' => $this->data,
      'total_amount' => $this->total_amount,
      'status' => ucfirst($this->status),
      'actions' => $actions,
      'statusUpdates' => $this->statusUpdates->map(function($item) { return $item->toCustomJson(); }),
      'user' => $this->user->toCustomJson(),
    ];
  }
}
