<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Meal extends Model {
  use HasFactory;

  public $timestamps = false;

  protected $fillable = ['name', 'description', 'price'];

  public function restaurant() {
    return $this->belongsTo(Restaurant::class, 'restaurant_id');
  }

  public function toCustomJson() {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'description' => $this->description,
      'price' => $this->price,
    ];
  }
}
