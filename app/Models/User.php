<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable {
  use HasFactory, Notifiable, HasApiTokens;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'email',
    'password',
    'blocked_at',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password',
    'remember_token',
    'blocked_at'
  ];

  /**
   * The attributes that should be cast to native types.
   *
   * @var array
   */
  protected $casts = [
    'email_verified_at' => 'datetime',
  ];


  protected $with = [
    'rights', 'roles'
  ];

  public function rights() {
    $relation = $this->belongsToMany(Right::class, 'user_rights', 'user_id', 'right_id');
    return $relation->withTimestamps();
  }

  public function roles() {
    $relation = $this->belongsToMany(Role::class, 'user_roles', 'user_id', 'role_id');
    return $relation->withTimestamps();
  }

  public function orders() {
    return $this->hasMany(Order::class, 'user_id');
  }

  public function isAdmin() {
    $role = $this->roles()->pluck('role_key')->first();
    return $role == 'restaurant_owner';
  }

  public function toCustomJson() {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'is_admin' => $this->isAdmin(),
      'blocked' => $this->blocked_at ? true : false,
    ];
  }
}
