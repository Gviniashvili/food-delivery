<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Right extends Model {
  public function users() {
    return $this->belongsToMany(User::class, 'user_rights', 'right_id', 'user_id');
  }

  public function resolveChildRouteBinding($childType, $value, $field) {
    return parent::resolveChildRouteBinding($childType, $value, $field);
  }
}
