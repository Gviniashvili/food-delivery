<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restaurant extends Model {
  use HasFactory;

  public $timestamps = false;

  protected $fillable = ['name', 'description'];

  public function meals() {
    return $this->hasMany(Meal::class, 'restaurant_id');
  }

  public function orders() {
    return $this->hasMany(Order::class, 'restaurant_id');
  }

  public function toCustomJson() {
    return [
      'id' => $this->id,
      'name' => $this->name,
      'description' => $this->description,
    ];
  }
}
