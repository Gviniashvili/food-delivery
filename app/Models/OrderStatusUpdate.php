<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderStatusUpdate extends Model {
  use HasFactory;

  public $timestamps = false;

  protected $dates = ['created_at'];

  protected $fillable = ['status'];

  public function order() {
    return $this->belongsTo(Order::class, 'order_id');
  }

  public function user() {
    return $this->belongsTo(User::class, 'user_id');
  }

  public function toCustomJson() {
    return [
      'date' => $this->created_at->format('F d, Y / H:i'),
      'status' => str_replace('_', ' ', ucfirst($this->status)),
      'user' => $this->user->toCustomJson(),
    ];
  }
}
