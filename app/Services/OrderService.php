<?php

namespace App\Services;

use App\Models\Meal;
use App\Models\Order;
use App\Models\OrderStatusUpdate;

class OrderService {
  public function createOrder($orderItems, $user): Order {
    $restaurant = null;
    $orderMeals = collect([]);
    $totalPrice = 0;
    foreach ($orderItems as $item) {
      $meal = Meal::findOrFail($item['id']); // Throws exception if not found
      if (!$restaurant) $restaurant = $meal->restaurant; // Take first meal's restaurant

      if ($restaurant->id != $meal->restaurant->id) {
        throw new \Exception("You can\'t order from multiple restaurants");
      }

      $mealData = $meal->toCustomJson();
      $orderMeals->push(array_merge($mealData, ['quantity' => $item['quantity']]));
      $totalPrice += $item['quantity'] * $meal->price;
    }

    $order = new Order([
      'data' => [
        'meals' => $orderMeals,
        'restaurant' => $restaurant->toCustomJson(),
      ],
      'total_amount' => $totalPrice,
      'status' => Order::PLACED
    ]);
    $order->restaurant()->associate($restaurant);
    $order->user()->associate($user);
    $order->save();

    $this->registerStatusUpdate($order, Order::PLACED, $user);

    return $order;
  }

  public function updateOrderStatus(Order $order, $status, $user) {
    $order->fill(['status' => $status])->save();
    $this->registerStatusUpdate($order, $status, $user);
  }

  public function registerStatusUpdate($order, $status, $user) {
    $statusUpdate = new OrderStatusUpdate(['status' => $status]);
    $statusUpdate->order()->associate($order);
    $statusUpdate->user()->associate($user);
    $statusUpdate->save();
  }
}